import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'allocator';

    constructor( private router: Router ) { }

    ngOnInit() {
        const splashScreen: HTMLElement = document.getElementById('splashScreenClass');
        if (splashScreen) {
            setTimeout(() => {
                splashScreen.remove();
            }, 2000);
        }
    }

    gotoDashboard(): void {
        this.router.navigate([ '/dashboard' ]);
    }
    
}